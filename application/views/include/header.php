<?php defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Weekender</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Learn any course in a weekend">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>styles/bootstrap4/bootstrap.min.css">
    <link href="<?php echo asset_url();?>plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/OwlCarousel2-2.2.1/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>plugins/OwlCarousel2-2.2.1/animate.css">
    <?php if($this->router->fetch_method() == 'index'){?>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>styles/main_styles.css">
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>styles/responsive.css">
    <?php }?>
    <?php if($this->router->fetch_method() == 'digital_marketing'){?>
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>styles/course.css">
        <link rel="stylesheet" type="text/css" href="<?php echo asset_url();?>styles/course_responsive.css">
    <?php }?>
</head>
<body>

<div class="super_container">

    <!-- Header -->

    <header class="header">

        <!-- Top Bar -->
        <div class="top_bar">
            <div class="top_bar_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
                                <div class="top_bar_phone">
                                    <span class="top_bar_title">phone:</span><?php echo $this->config->item('admin_phone_number');?>
                                    <span class="top_bar_title">&nbsp;email:</span><?php echo $this->config->item('admin_email');?>
                                </div>
                                <div class="top_bar_right ml-auto">

                                    <!-- Social -->
                                    <div class="top_bar_social">
                                        <span class="top_bar_title social_title">follow us</span>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li class="d-none"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li class="d-none"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header Content -->
        <div class="header_container">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header_content d-flex flex-row align-items-center justify-content-start">
                            <div class="logo_container mr-auto">
                                <a href="<?php echo site_url();?>">
                                    <img class="logo_img" src="<?php echo asset_url();?>images/weekender-logo.png" alt="Weekender Logo">
                                    <div class="logo_text">
                                        Weekender
                                    </div>
                                </a>
                            </div>
                            <nav class="main_nav_contaner">
                                <ul class="main_nav">
                                    <li class="active d-none"><a href="<?php echo site_url();?>">Home</a></li>
                                    <li class="d-none"><a href="javascript:;">Courses</a></li>
                                    <li class="d-none"><a href="instructors.html">Instructors</a></li>
                                    <li class="d-none"><a href="#">Events</a></li>
                                    <li class="d-none"><a href="blog.html">Blog</a></li>
                                    <li class="d-none"><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                            <div class="header_content_right ml-auto text-right d-none">
                                <div class="header_search">
                                    <div class="search_form_container">
                                        <form action="#" id="search_form" class="search_form trans_400">
                                            <input type="search" class="header_search_input trans_400" placeholder="Type for Search" required="required">
                                            <div class="search_button">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <!-- Hamburger -->

                                <div class="user"><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></div>
                                <div class="hamburger menu_mm">
                                    <i class="fa fa-bars menu_mm" aria-hidden="true"></i>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <!-- Menu -->

    <div class="menu d-flex flex-column align-items-end justify-content-start text-right menu_mm trans_400">
        <div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
        <div class="search d-none">
            <form action="#" class="header_search_form menu_mm">
                <input type="search" class="search_input menu_mm" placeholder="Search" required="required">
                <button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
                    <i class="fa fa-search menu_mm" aria-hidden="true"></i>
                </button>
            </form>
        </div>
        <nav class="menu_nav">
            <ul class="menu_mm">
                <li class="menu_mm d-none"><a href="<?php echo site_url();?>">Home</a></li>
                <li class="menu_mm d-none"><a href="javascript:;">Courses</a></li>
                <li class="menu_mm d-none"><a href="instructors.html">Instructors</a></li>
                <li class="menu_mm d-none"><a href="#">Events</a></li>
                <li class="menu_mm d-none"><a href="blog.html">Blog</a></li>
                <li class="menu_mm d-none"><a href="contact.html">Contact</a></li>
            </ul>
        </nav>
        <div class="menu_extra">
            <div class="menu_phone"><span class="menu_title">phone:</span><?php echo $this->config->item('admin_phone_number');?></div>
            <div class="menu_social">
                <span class="menu_title">follow us</span>
                <ul>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li class="d-none"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <li class="d-none"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Home -->