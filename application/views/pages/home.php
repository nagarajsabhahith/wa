<?php $this->load->view('include/header'); ?>

    <div class="home">
        <div class="home_background" style="background-image: url('<?php echo asset_url();?>images/index_background.jpg');"></div>
        <div class="home_content">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h1 class="home_title">Learn any course over a weekend</h1>
                        <div class="home_button trans_200"><a id="getingStarted" href="javascript:;">get started</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Courses -->

    <div id="courses" class="courses">
        <div class="courses_background"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="section_title text-center">Courses Offered</h2>
                </div>
            </div>
            <div class="row courses_row">

                <!-- Course -->
                <div class="col-lg-4 course_col">
                    <div class="course">
                        <div class="course_image">
                            <a href="<?php echo site_url();?>courses/digital-marketing">
                                <img src="<?php echo asset_url();?>images/course_1.jpg" alt=""></div>
                            </a>
                        <div class="course_body">
                            <div class="course_title"><a href="<?php echo site_url();?>courses/digital-marketing">Digital Marketing</a></div>
                            <div class="course_info">
                                <ul>
                                    <li><a href="javascript:;">Sekhar Radha</a></li>
                                    <li><a href="#">5+ years of experience</a></li>
                                </ul>
                            </div>
                            <div class="course_text">
                                <p>Prerequisites: No prior knowledge of Digital Marketing is required</p>
                            </div>
                        </div>
                        <div class="course_footer d-flex flex-row align-items-center justify-content-start">
                            <div class="course_students"><i class="fa fa-user" aria-hidden="true"></i><span>10</span></div>
                            <div class="course_rating ml-auto"><i class="fa fa-star" aria-hidden="true"></i><span>4.9</span></div>
                            <div class="course_mark course_free trans_200"><a href="#">Rs. 4999</a></div>
                        </div>
                    </div>
                </div>

                <!-- Course -->
                <div class="col-lg-4 course_col">
                    <div class="course">
                        <div class="course_image"><img src="<?php echo asset_url();?>images/course_2.jpg" alt=""></div>
                        <div class="course_body">
                            <div class="course_title"><a href="javascript:;">Google Adwords</a></div>
                            <div class="course_info">
                                <ul>
                                    <li><a href="javascript:;">Nagaraj S</a></li>
                                    <li><a href="#">5+ years of experience</a></li>
                                </ul>
                            </div>
                            <div class="course_text">
                                <p>Prerequisites: Basic knowledge of Digital Marketing is required</p>
                            </div>
                        </div>
                        <div class="course_footer d-flex flex-row align-items-center justify-content-start">
                            <div class="course_students"><i class="fa fa-user" aria-hidden="true"></i><span>0</span></div>
                            <div class="course_rating ml-auto"><i class="fa fa-star" aria-hidden="true"></i><span>0</span></div>
                            <div class="course_mark trans_200"><a href="javascript:;">Soon</a></div>
                        </div>
                    </div>
                </div>

                <!-- Course -->
                <div class="col-lg-4 course_col">
                    <div class="course">
                        <div class="course_image"><img src="<?php echo asset_url();?>images/course_3.jpg" alt=""></div>
                        <div class="course_body">
                            <div class="course_title"><a href="javascript:;">SEO</a></div>
                            <div class="course_info">
                                <ul>
                                    <li><a href="javascript:;">Shikha Jain</a></li>
                                    <li><a href="#">3+ years of experience</a></li>
                                </ul>
                            </div>
                            <div class="course_text">
                                <p>Prerequisites: Basic knowledge of Digital Marketing is required</p>
                            </div>
                        </div>
                        <div class="course_footer d-flex flex-row align-items-center justify-content-start">
                            <div class="course_students"><i class="fa fa-user" aria-hidden="true"></i><span>0</span></div>
                            <div class="course_rating ml-auto"><i class="fa fa-star" aria-hidden="true"></i><span>0</span></div>
                            <div class="course_mark trans_200"><a href="javascript:;">Soon</a></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Instructors -->

    <div class="instructors">
        <div class="instructors_background" style="background-image:url('<?php echo asset_url();?>images/instructors_background.png')"></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="section_title text-center">The Best Tutors in Town</h2>
                </div>
            </div>
            <div class="row instructors_row">

                <!-- Instructor -->
                <div class="col-lg-4 instructor_col">
                    <div class="instructor text-center">
                        <div class="instructor_image_container">
                            <div class="instructor_image"><img src="<?php echo asset_url();?>images/instructor_1.jpg" alt=""></div>
                        </div>
                        <div class="instructor_name"><a href="javascript:;">Sekhar Radha</a></div>
                        <div class="instructor_title">Tutor</div>
                        <div class="instructor_text">
                            <p>Graduated from IIT Madras. 5+ years of Digital Marketing experience. Mentored few startups</p>
                        </div>
                        <div class="instructor_social">
                            <ul>
                                <!--
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                -->
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Instructor -->
                <div class="col-lg-4 instructor_col">
                    <div class="instructor text-center">
                        <div class="instructor_image_container">
                            <div class="instructor_image"><img src="<?php echo asset_url();?>images/instructor_2.jpg" alt=""></div>
                        </div>
                        <div class="instructor_name"><a href="javascript:;">Nagaraj S</a></div>
                        <div class="instructor_title">Tutor</div>
                        <div class="instructor_text">
                            <p>5+ years of Digital Marketing experience. Own a Digital Marketing agency. Done consulting for few startups</p>
                        </div>
                        <div class="instructor_social">
                            <ul>

                                <!--
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                -->
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Instructor -->
                <div class="col-lg-4 instructor_col">
                    <div class="instructor text-center">
                        <div class="instructor_image_container">
                            <div class="instructor_image"><img src="<?php echo asset_url();?>images/instructor_3.jpg" alt=""></div>
                        </div>
                        <div class="instructor_name"><a href="javascript:;">Shikha Jain</a></div>
                        <div class="instructor_title">Tutor</div>
                        <div class="instructor_text">
                            <p>3+ years of Digital Marketing. Content Ninja. Sound knowledge of Search Engines. Done quite a few strategic partnerships. </p>
                        </div>
                        <div class="instructor_social">
                            <ul>
                                <!--
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                -->
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Register -->

    <div class="register">
        <div class="container">
            <div class="row">

                <!-- Register Form -->

                <div class="col-lg-12">
                    <div class="register_form_container">
                        <div class="register_form_title">Registration</div>
                        <form action="javascript:;" id="register_form" class="register_form">
                            <div class="row register_row">
                                <div id="successMessage" class="d-none col-sm-12">
                                    <div id="successAlert" class="alert alert-success alert-dismissible" role="alert">
                                        We have received your message. We will get back to you shortly.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <div id="errorMessage" class="d-none col-sm-12">
                                    <div id="errorAlert" class="alert alert-warning alert-dismissible" role="alert">
                                        Something went wrong please try after some time!
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-lg-6 register_col">
                                    <input id="name" type="text" class="form_input" placeholder="Name" required="required">
                                </div>
                                <div class="col-lg-6 register_col">
                                    <input id="email" type="email" class="form_input" placeholder="Email" required="required">
                                </div>
                                <div class="col-lg-6 register_col">
                                    <input required="required" id="phone" type="tel" class="form_input" placeholder="Phone">
                                </div>
                                <div class="col-lg-6 register_col">
                                    <select id="course" required="required" class="form_input" placeholder="Courses">
                                        <option value="Digital Marketing">Digital Marketing</option>
                                        <option value="Google Adwords">Google Adwords</option>
                                        <option value="SEO">SEO</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <button id="registerNowBtn" class="form_button trans_200">Register Now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Register Timer -->

                <div class="col-lg-6 d-none">
                    <div class="register_timer_container">
                        <div class="register_timer_title">Register Now</div>
                        <div class="register_timer_text">
                            <p>Register for these courses before friday 5:00 PM</p>
                        </div>
                        <div class="timer_container d-none">
                            <ul class="timer_list">
                                <li><div id="day" class="timer_num">00</div><div class="timer_ss">days</div></li>
                                <li><div id="hour" class="timer_num">00</div><div class="timer_ss">hours</div></li>
                                <li><div id="minute" class="timer_num">00</div><div class="timer_ss">minutes</div></li>
                                <li><div id="second" class="timer_num">00</div><div class="timer_ss">seconds</div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $this->load->view('include/footer'); ?>