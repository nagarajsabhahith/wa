<?php $this->load->view('include/header'); ?>
<div class="home">
    <div class="breadcrumbs_container">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumbs_list d-flex flex-row align-items-center justify-content-start">
                        <li><a href="<?php echo site_url();?>">home</a></li>
                        <li><a href="javascript:;">courses</a></li>
                        <li>digital marketing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Intro -->

<div class="intro">
    <div class="intro_background parallax-window" data-parallax="scroll" data-image-src="<?php echo asset_url();?>images/intro.jpg" data-speed="0.8"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="intro_container d-flex flex-column align-items-start justify-content-end">
                    <div class="intro_content">
                        <div class="intro_price">Rs. 4999</div>
                        <div class="rating_r rating_r_4 intro_rating"><i></i><i></i><i></i><i></i><i></i></div>
                        <div class="intro_title">Digital Marketing</div>
                        <div class="intro_meta">
                            <div class="intro_image"><img src="<?php echo asset_url();?>images/intro_user.png" alt=""></div>
                            <div class="intro_instructors"><a href="javascript:;">Sekhar Radha</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Course -->

<div class="course">
    <div class="course_top"></div>
    <div class="container">
        <div class="row row-lg-eq-height">

            <!-- Panels -->
            <div class="col-lg-9">
                <div class="tab_panels">

                    <!-- Tabs -->
                    <div class="course_tabs_container">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="tabs d-flex flex-row align-items-center justify-content-start">
                                        <div class="tab active">description</div>
                                        <div class="tab">curriculum</div>
                                        <div id="registrationTabLink" class="tab">registration</div>
<!--                                        <div class="tab">members</div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Description -->
                    <div class="tab_panel description active">
                        <div class="panel_title">About this course</div>
                        <div class="panel_text">
                            <p>
                                Digital Marketing is the constantly evolving field having its presence all around the world. It continues to grow rapidly, and the marketers are facing new challenges and opportunities to expose themselves in this digital age! We, at Weekender Academy, offer digital marketing training in Bengaluru which provides a knowledge-sharing platform for fresher's and marketing professionals that increases the valuable efforts towards the success of their career/business. We are a predominant digital marketing institute in Bengaluru. Our classroom-based digital marketing training in Bengaluru will provide practical exposure to gain the real-world understanding of digital technologies. You will learn the techniques to make a successful marketing campaign.
                            </p>
                        </div>

                        <!-- Instructors -->
                        <div class="instructors">
                            <div class="panel_title">Tutor</div>
                            <div class="row instructors_row">

                                <!-- Instructor -->
                                <div class="col-lg-4 col-md-6">
                                    <div class="instructor d-flex flex-row align-items-center justify-content-start">
                                        <div class="instructor_image"><div><img src="<?php echo asset_url();?>images/instructor_1.jpg" alt=""></div></div>
                                        <div class="instructor_content">
                                            <div class="instructor_name"><a href="javascript:;">Sekhar Radha</a></div>
                                            <div class="instructor_title">Tutor
                                            <small class="d-block">Graduated from IIT Madras. 5+ years of Digital Marketing experience. Mentored few startups.</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- FAQs -->
                        <div class="faqs">
                            <div class="panel_title">FAQs</div>
                            <div class="accordions">

                                <div class="elements_accordions">


                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center active">
                                            <div>
                                                Why should I choose Weekender Academy?
                                            </div>
                                        </div>
                                        <div class="accordion_panel">
                                            <p>
                                                We have got Industry best experts to teach you this course, we will make you work on real-world problems during the course which would gain you exposure and make you proficient in whatever you are learning.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center">
                                            <div>
                                                What will I accomplish after doing this training?
                                            </div>
                                        </div>
                                        <div class="accordion_panel">
                                            <p>
                                                You will gain right knowledge to run marketing campaigns on all social media platforms, do online advertising and create Ad networks, Implement right techniques of SEO, Work with social media for best results, track each and every conversion happening on any online channels.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center">
                                            <div>
                                                What background knowledge is necessary?
                                            </div></div>
                                        <div class="accordion_panel">
                                            <p>
                                                No prior knowledge is required in order to take this course.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center"><div>
                                                How many hours of classes will be conducted?
                                            </div></div>
                                        <div class="accordion_panel">
                                            <p>
                                                Classes will be conducted only during weekend, course span will be of 16 - 24 hours.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center"><div>
                                                What would be the course composition?
                                            </div></div>
                                        <div class="accordion_panel">
                                            <p>
                                                Course comprises of Theory, practical examples, quizzes to enhance your learning.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center"><div>
                                                How do I reach you guys before opting for the course?
                                            </div></div>
                                        <div class="accordion_panel">
                                            <p>
                                                You can mail us on care@weekenderacademy.com or call us on +91 8056127417 for more details.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="accordion_container">
                                        <div class="accordion d-flex flex-row align-items-center"><div>
                                                Where is your academy located?
                                            </div></div>
                                        <div class="accordion_panel">
                                            <p>
                                                Currently we are based out of 91 springboard, Koramangala which is next to Forum mall.
                                            </p>
                                        </div>
                                    </div>



















                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Curriculum -->
                    <div class="tab_panel curriculum">
                        <div class="panel_title">Syllabus</div>
<!--                        <div class="panel_text">-->
<!--                            <p>Later</p>-->
<!--                        </div>-->
                        <div class="curriculum_items">
                            <div class="cur_item">
                                <div class="cur_title_container d-flex flex-row align-items-start justify-content-start">
                                    <div class="cur_title">Day 1</div>
<!--                                    <div class="cur_num ml-auto">0/4</div>-->
                                </div>
                                <div class="cur_item_content">
                                    <div class="cur_item_title">Beginners Level</div>
<!--                                    <div class="cur_item_text">-->
<!--                                        <p>Nam egestas lorem ex, sit amet commodo tortor faucibus a. Suspendisse commodo, turpis a dapibus fermentum, turpis ipsum rhoncus massa, sed commodo nisi lectus id ipsum.</p>-->
<!--                                    </div>-->
                                    <div class="cur_contents">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>OVERVIEW</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Digital Marketing Intro</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Google Tag Manager</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>SEO</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Search Engine optimization</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Webmaster tools</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>GOOGLE ADWORDS</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Google Ads</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>2 hours</span></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>FACEBOOK ADS</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Facebook Ads</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Instagram Ads</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>1 hour</span></div>
                                                    </li>

                                                </ul>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="cur_item">
                                <div class="cur_title_container d-flex flex-row align-items-start justify-content-start">
                                    <div class="cur_title">Day 2</div>
<!--                                    <div class="cur_num ml-auto">0/4</div>-->
                                </div>
                                <div class="cur_item_content">
                                    <div class="cur_item_title">Beginners Level</div>
<!--                                    <div class="cur_item_text">-->
<!--                                        <p>Nam egestas lorem ex, sit amet commodo tortor faucibus a. Suspendisse commodo, turpis a dapibus fermentum, turpis ipsum rhoncus massa, sed commodo nisi lectus id ipsum.</p>-->
<!--                                    </div>-->
                                    <div class="cur_contents">
                                        <ul>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>COMMUNICATIONS</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Sms Marketing</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Email Marketing</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Push notifications</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>

                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>Google Analytics</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Google Analytics</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>2 hours</span></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>Types of Marketing</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Affiliate Marketing</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Content Marketing</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Influencer Marketing</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>40 Mins</span></div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i class="fa fa-file" aria-hidden="true"></i><span>SUMMARY</span>
                                                <ul>
                                                    <li class="d-flex flex-row align-items-center justify-content-start">
                                                        <i class="fa fa-file" aria-hidden="true"></i><span>Career opportunities</span>
                                                        <div class="cur_time ml-auto"><i class="fa fa-clock-o" aria-hidden="true"></i><span>2 hours</span></div>
                                                    </li>

                                                </ul>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Members -->
                    <div class="tab_panel registration">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="register_form_container">
                                    <div class="register_form_title">Registration</div>
                                    <form action="javascript:;" id="register_form" class="register_form">
                                        <div class="row register_row">
                                            <div id="successMessage" class="d-none col-sm-12">
                                                <div id="successAlert" class="alert alert-success alert-dismissible" role="alert">
                                                    We have received your message. We will get back to you shortly.
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div id="errorMessage" class="d-none col-sm-12">
                                                <div id="errorAlert" class="alert alert-warning alert-dismissible" role="alert">
                                                    Something went wrong please try after some time!
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 register_col">
                                                <input id="name" type="text" class="form_input" placeholder="Name" required="required">
                                            </div>
                                            <div class="col-lg-12 register_col">
                                                <input id="email" type="email" class="form_input" placeholder="Email" required="required">
                                            </div>
                                            <div class="col-lg-12 register_col">
                                                <input id="phone" required="required" type="tel" class="form_input" placeholder="Phone">
                                            </div>
                                            <div class="col">
                                                <button id="registerNowBtn" class="form_button trans_200">Register Now</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-lg-3">
                <div class="sidebar">
                    <div class="sidebar_background"></div>
                    <div class="sidebar_top"><a id="registerCourse" href="javascript:;">register course</a></div>
                    <div class="sidebar_content">

                        <!-- Features -->
                        <div class="sidebar_section features">
                            <div class="sidebar_title">Course Features</div>
                            <div class="features_content">
                                <ul class="features_list">

                                    <!-- Feature -->
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div class="feature_title"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Duration</span></div>
                                        <div class="feature_text ml-auto">16 - 24 Hours</div>
                                    </li>

                                    <!-- Feature -->
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div class="feature_title"><i class="fa fa-bell" aria-hidden="true"></i><span>Lectures</span></div>
                                        <div class="feature_text ml-auto">8</div>
                                    </li>

                                    <!-- Feature -->
                                    <li class="d-flex flex-row align-items-start justify-content-start">
                                        <div class="feature_title"><i class="fa fa-id-badge" aria-hidden="true"></i><span>Quizzes</span></div>
                                        <div class="feature_text ml-auto">8</div>
                                    </li>

                                    <!-- Feature -->
<!--                                    <li class="d-flex flex-row align-items-start justify-content-start">-->
<!--                                        <div class="feature_title"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>Pass Percentage</span></div>-->
<!--                                        <div class="feature_text ml-auto">60</div>-->
<!--                                    </li>-->

                                    <!-- Feature -->
<!--                                    <li class="d-flex flex-row align-items-start justify-content-start">-->
<!--                                        <div class="feature_title"><i class="fa fa-thumbs-down" aria-hidden="true"></i><span>Max Retakes</span></div>-->
<!--                                        <div class="feature_text ml-auto">5</div>-->
<!--                                    </li>-->
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('include/footer'); ?>